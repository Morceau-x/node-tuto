import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import { Database } from './database';
require('dotenv').config();

const app = express();
app.use(bodyParser.json());
const database = new Database();

app.get('/', (req: Request, res: Response) => res.send('Hello World!'));

app.get('/user/:id', async function(req: Request, res: Response) {
    try {
        res.send(await database.getUserById(req.params.id));
    } catch (e) {
        res.statusCode = 400;
        res.send(e.message);
    }
});

app.get('/users', async function(req: Request, res: Response) {
    try {
        res.send(await database.getAllUsers());
    } catch (e) {
        res.statusCode = 400;
        res.send(e.message);
    }
});

app.post('/users', async function(req: Request, res: Response) {
    try {
        res.send(await database.addUsers(req.body));
    } catch (e) {
        res.statusCode = 400;
        res.send(e.message);
    }
});

app.listen(process.env.port, () => console.log(`App listening on port ${process.env.port}!`));
