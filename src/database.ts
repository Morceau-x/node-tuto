import Knex from 'knex';

const environment = process.env.ENVIRONMENT || 'development';
const defaultConfig = require('../knexfile.js')[environment];

export interface User {
    id?: string;
    firstname: string;
    lastname: string;
}

export class Database {
    knex: Knex;

    constructor(config: object = defaultConfig) {
        this.knex = require('knex')(config);
    }

    async getUserById(id: string): Promise<User> {
        return this.knex('users')
            .where({ id: id })
            .first();
    }

    async getAllUsers(): Promise<User[]> {
        console.log('get all users');
        return this.knex('users').select('*');
    }

    async addUsers(data: User | User[]): Promise<any> {
        return this.knex('users').insert(data);
    }
}
