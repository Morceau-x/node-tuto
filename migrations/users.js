
exports.up = function(knex) {
    return knex.schema.createTableIfNotExists('users', function(table) {
        table.increments('id').primary('id');
        table.string('firstname').notNullable();
        table.string('lastname').notNullable();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists('users')
};
