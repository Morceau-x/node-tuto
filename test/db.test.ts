import { Database } from '../src/database';
const config = require('../knexfile.js')['test'];

const database: Database = new Database(config);

test('Always successful test', () => {});

test('Add user', async () => {
    await database.knex.migrate.latest();
    await database
        .knex('users')
        .whereRaw('true')
        .del();
    await database.addUsers({ firstname: 'test1', lastname: 'test2' });
    expect(await database.knex.count()).toEqual([{ 'count(*)': 1 }]);
});

test('Get user', async () => {
    await database.knex.migrate.latest();
    await database.knex('users').del();
    const [id] = await database.addUsers({ firstname: 'test1', lastname: 'test2' });
    expect(await database.getUserById(id)).toEqual({ firstname: 'test1', lastname: 'test2', id: id });
});

test('Get all users', async () => {
    await database.knex.migrate.latest();
    await database.knex('users').del();
    const [id1] = await database.addUsers({ firstname: 'test1', lastname: 'test2' });
    const [id2] = await database.addUsers({ firstname: 'test1', lastname: 'test2' });
    const [id3] = await database.addUsers({ firstname: 'test1', lastname: 'test2' });
    expect(await database.getAllUsers()).toEqual([
        { firstname: 'test1', lastname: 'test2', id: id1 },
        { firstname: 'test1', lastname: 'test2', id: id2 },
        { firstname: 'test1', lastname: 'test2', id: id3 }
    ]);
});
